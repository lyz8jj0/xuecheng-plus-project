package com.xuecheng.media.service.jobhandler;

import com.xuecheng.base.utils.Mp4VideoUtil;
import com.xuecheng.media.model.po.MediaProcess;
import com.xuecheng.media.service.MediaFileProcessService;
import com.xuecheng.media.service.MediaFileService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.M
 * @version 1.0
 * @description 测试执行器
 * @date 2022/9/13 20:32
 */
@Component
@Slf4j
public class SampleXxlJob {

    @Autowired
    MediaFileProcessService mediaFileProcessService;

    @Value("${videoprocess.ffmpegpath}")
    private String ffmpegPath;

    @Autowired
    MediaFileService mediaFileService;

    /**
     * 视频处理任务
     */
    @XxlJob("videoJobHandler")
    public void videoJobHandler() throws Exception {

        // 分片参数
        int shardIndex = XxlJobHelper.getShardIndex(); // 执行器的序号, 从0开始
        int shardTotal = XxlJobHelper.getShardTotal(); // 执行器的总数

        log.info("分片参数：当前分片序号 = {}, 总分片数 = {}", shardIndex, shardTotal);
        log.info("开始执行第" + shardIndex + "批任务");

        // 确定cpu的核心数
        int processors = Runtime.getRuntime().availableProcessors();

        // 查询待处理的任务
        List<MediaProcess> mediaProcessList = mediaFileProcessService.getMediaProcessList(shardIndex, shardTotal, processors);
        int size = mediaProcessList.size();
        log.debug("取到视频处理任务数:{}", size);
        if (size == 0) {
            return;
        }
        // 创建一个线程池
        ExecutorService executorService = Executors.newFixedThreadPool(size);
        // 使用的计数器
        CountDownLatch countDownLatch = new CountDownLatch(size);
        mediaProcessList.forEach(mediaProcess -> {

            executorService.execute(() -> {
                try {
                    Long taskId = mediaProcess.getId();
                    // 文件id就是md5
                    String fileId = mediaProcess.getFileId();
                    // 开启任务
                    boolean status = mediaFileProcessService.startTask(taskId);
                    if (!status) {
                        log.debug("任务已经被其他机器执行，本机不再执行，任务id:{}", taskId);
                        return;
                    }
                    // 桶
                    String bucket = mediaProcess.getBucket();
                    String objectName = mediaProcess.getFilePath();
                    // 下载文件到本地
                    File file = mediaFileService.downloadFileFromMinIO(bucket, objectName);
                    if (file == null) {
                        log.debug("下载任务出错, 任务id:{}, bucket:{},objectName:{}", taskId, bucket, objectName);
                        mediaFileProcessService.saveProcessFinishStatus(taskId, "3", fileId, null, "下载文件到本地失败");
                        return;
                    }

                    // 转换后mp4文件的名称
                    String mp4_name = fileId + ".mp4";
                    // 源avi视频的路径
                    String video_path = file.getAbsolutePath();
                    // 转换后mp4文件的路径
                    // 先创建一个临时文件, 作为转换后的文件
                    File mp4File = null;
                    try {
                        mp4File = File.createTempFile(fileId, ".mp4");
                    } catch (IOException e) {
                        log.debug("创建临时文件失败, 任务id:{}", taskId);
                        mediaFileProcessService.saveProcessFinishStatus(taskId, "3", fileId, null, "创建临时文件失败");

                        return;
                    }
                    String mp4_path = mp4File.getAbsolutePath();
                    // 创建工具类对象
                    Mp4VideoUtil videoUtil = new Mp4VideoUtil(ffmpegPath, video_path, mp4_name, mp4_path);
                    // 开始视频转化, 成功将返回success
                    String result = videoUtil.generateMp4();
                    if (!result.equals("success")) {
                        log.debug("视频转码失败, 原因:{}, bucket:{}, objectName:{}", result, bucket, objectName);
                        mediaFileProcessService.saveProcessFinishStatus(taskId, "3", fileId, null, "视频转码失败");
                        return;
                    }
                    // 上传到minio
                    // mp4文件url
                     objectName = getFilePath(fileId, ".mp4");
                    //访问url
                    String url = "/" + bucket + "/" + objectName;
                    boolean b1 = mediaFileService.addMediaFilesToMinIO(mp4File.getAbsolutePath(), "video/mp4", bucket, objectName);
                    if (!b1) {
                        log.debug("上传mp4到minio失败,taskId:{}, bucket:{}, objectName:{}", taskId, bucket, objectName);
                        mediaFileProcessService.saveProcessFinishStatus(taskId, "3", fileId, null, "上传mp4到minio失败");
                        return;
                    }

                    // 更新任务状态为成功
                    mediaFileProcessService.saveProcessFinishStatus(taskId, "2", fileId, url, null);
                } finally {
                    // 计数器减1
                    countDownLatch.countDown();
                }
            });
        });

        // 阻塞, 指定最大限制的等待时间, 阻塞最多等待一定的时间后就解除阻塞
        countDownLatch.await(30, TimeUnit.MINUTES);

    }

    private String getFilePath(String fileMd5, String fileExt) {
        return fileMd5.substring(0, 1) + "/" + fileMd5.substring(1, 2) + "/" + fileMd5 + "/" + fileMd5 + fileExt;
    }
}