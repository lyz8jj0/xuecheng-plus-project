package com.xuecheng.media;


import io.minio.ComposeObjectArgs;
import io.minio.ComposeSource;
import io.minio.UploadObjectArgs;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.xuecheng.media.MinioTest.minioClient;

public class BigFileTest {
    // 分块测试
    @Test
    public void testChunk() throws IOException {
        File sourceFile = new File("/Users/lixinyu/Downloads/2087_1698205465_20231025115706A002_去重.mp4");
        String chunkPath = "/Users/lixinyu/学习项目案例/upload/chunk/";
        // 定义分块大小
        int chunkSize = 5 * 1024 * 1024;
        // 分块数量
        int chunkNum = (int) Math.ceil(sourceFile.length() * 1.0 / chunkSize);
        // 使用流从源文件读数据, 向分块文件中写数据
        RandomAccessFile raf_r = new RandomAccessFile(sourceFile, "r");
        // 缓存区
        byte[] bytes= new byte[1024];
        for (int i = 0; i < chunkNum; i++) {
            File chunkFile = new File(chunkPath + i);
            // 分块文件写入流
            RandomAccessFile raf_rw = new RandomAccessFile(chunkFile, "rw");
            int len = -1;
            while ((len = raf_r.read(bytes)) != -1) {
                raf_rw.write(bytes, 0, len);
                if (chunkFile.length() >= chunkSize) {
                    break;
                }
            }
            raf_rw.close();
        }
        raf_r.close();

    }

    // 将分块进行合并
    @Test
    public void testMerge() throws IOException {
        // 块文件目录
        File chunkFolder = new File("/Users/lixinyu/学习项目案例/upload/chunk/");
        // 源文件
        File sourceFile = new File("/Users/lixinyu/Downloads/2087_1698205465_20231025115706A002_去重.mp4");
        // 合并后的文件
        File mergeFile = new File("/Users/lixinyu/Downloads/2087_1698205465_20231025115706A002_去重_2.mp4");

        // 取出所有块
        File[] files = chunkFolder.listFiles();
        List<File> fileList = Arrays.asList(files);
        Collections.sort(fileList, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return Integer.parseInt(o1.getName()) - Integer.parseInt(o2.getName());
            }
        });
        // 向合并文件写的流
        RandomAccessFile raf_rw = new RandomAccessFile(mergeFile, "rw");
        // 缓存区
        byte[] bytes = new byte[1024];

        // 合并文件
        for (File file : fileList) {
            // 读分场的流
            RandomAccessFile raf_r = new RandomAccessFile(file, "r");
            int len = -1;
            while ((len = raf_r.read(bytes)) != -1) {
                raf_rw.write(bytes, 0, len);
            }
            raf_r.close();
        }
        raf_rw.close();

        // 合并文件完成后对合并的文件进行校验
        FileInputStream fileInputStream_merge = new FileInputStream(mergeFile);
        FileInputStream fileInputStream_source = new FileInputStream(sourceFile);
        String md5_merge = DigestUtils.md5Hex(fileInputStream_merge);
        String md5_source = DigestUtils.md5Hex(fileInputStream_source);
        if (md5_merge.equals(md5_source)) {
            System.out.println("文件合并成功");
        }
    }

    // 将分块文件上传到minio
    @Test
    public void uploadChunk(){
        String chunkFolderPath = "/Users/lixinyu/学习项目案例/upload/chunk/";
        File chunkFolder = new File(chunkFolderPath);
        //分块文件
        File[] files = chunkFolder.listFiles();
        //将分块文件上传至minio
        for (int i = 0; i < files.length; i++) {
            try {
                UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder()
                        .bucket("testbucket")
                        .object("chunk/" + i)
                        .filename(files[i].getAbsolutePath())
                        .build();
                minioClient.uploadObject(uploadObjectArgs);
                System.out.println("上传分块成功"+i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    // 调用minio接口合并分块
    @Test
    public void test_merge() throws Exception{
        List<ComposeSource> sources = Stream.iterate(0, i -> ++i)
                .limit(2)
                .map(i -> ComposeSource.builder()
                        .bucket("testbucket")
                        .object("chunk/".concat(Integer.toString(i)))
                        .build())
                .collect(Collectors.toList());

        ComposeObjectArgs composeObjectArgs = ComposeObjectArgs.builder()
                .bucket("testbucket")
                .object("merge01.mp4")
                .sources(sources)
                .build();
        minioClient.composeObject(composeObjectArgs);
    }

    // 批量清理分块
}
